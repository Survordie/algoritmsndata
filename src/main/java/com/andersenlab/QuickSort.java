package com.andersenlab;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class QuickSort {

	public static void main(String[] args) {
		System.out.println(quickSort(Arrays.asList(7, 5, 9, 3, 7, 2)));
	}

	public static List<Integer> quickSort(List<Integer> list) {

		if (list.size() < 2) {
			return list;
		} else {
			Integer pivot = list.get(0);

			List<Integer> less = new ArrayList<>();
			List<Integer> greater = new ArrayList<>();

			for (int i = 1; i < list.size(); i++) {
				if (list.get(i) < pivot) {
					less.add(list.get(i));
				} else {
					greater.add(list.get(i));
				}
			}
			List<Integer> tempLess = quickSort(less);
			tempLess.add(pivot);
			List<Integer> tempGreater =quickSort(greater);
			tempLess.addAll(tempGreater);
			return tempLess;
		}
	}

}
