package com.andersenlab;

import java.util.Arrays;
import java.util.List;

public class Recursion {

	public static void main(String[] args) {

		System.out.println("Sum of elements of list is: " + sum(Arrays.asList(1L, 3L, 7L)));

		System.out.println("Amount of list is: " + countElements(Arrays.asList("1", "2")));

		System.out.println("Maximal element of list is: " + maxElements(Arrays.asList(3L, 2L, 7L)));

	}

	public static Long sum(List<Long> list) {
		if (list == null || list.size() == 0) {
			return 0L;
		}

		if (list.size() == 1) {
			return list.get(0);
		} else {
			return list.get(list.size() - 1) + sum(list.subList(0, list.size() - 1));
		}
	}

	public static <E> Integer countElements(List<E> list) {
		if (list == null || list.size() == 0) {
			return 0;
		}

		if (list.size() == 1) {
			return 1;
		} else {
			return 1 + countElements(list.subList(0, list.size() - 1));
		}
	}

	public static Long maxElements(List<Long> list) {
		if (list == null || list.size() == 0) {
			return null;
		}

		if (list.size() == 1) {
			return list.get(0);
		} else {
			Long tempElement = maxElements(list.subList(0, list.size() - 1));
			Long currentElement = list.get(list.size() - 1);

			return currentElement > tempElement ? currentElement : tempElement;
		}

	}

}
