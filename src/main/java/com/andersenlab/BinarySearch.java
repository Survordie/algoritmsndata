package com.andersenlab;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class BinarySearch {

	public static void main(String[] args) {
		List<Long> list = Arrays.asList(1L, 3L, 5L, 7L, 9L);
		Long number = 9L;
		System.out.println(binary_search(list, number));

	}

	public static Long binary_search(List<Long> list, Long item) {
		int low = 0;
		int high = list.size() - 1;

		while (low <= high) {
			int mid = (low + high) / 2;
			Long guess = list.get(mid);

			if (guess == item) {
				return guess;
			}
			if (guess > item) {
				high = mid - 1;
			} else {
				low = mid + 1;
			}

		}

		return null;

	}
}
